export const getMenuItemsReducer = (state={menus : []}, action) =>{
    switch(action.type)
    {
        case 'GET_MENU_REQUEST':
            return{
                loading : true,
                ...state
            }
        case 'GET_MENU_SUCCESS':
            return{
                loading : false,
                menus:action.payload
            }
        case 'GET_MENU_FAILED':
            return{
                error:action.payload,
                loading : false
            }
        default:
            return state;
    }
}
