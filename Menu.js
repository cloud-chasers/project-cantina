import React, {useState} from 'react'
// import menuData from '../menudata';
import {Modal} from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux';
import {addToCart} from "../actions/cartActions";


export default function Menu({menuInfo}) {
  const [ quantity, setquantity ] = useState(1); 
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const dispatch = useDispatch()
  function addtocart()
  {
    
    dispatch(addToCart(menuInfo,quantity))
  }

  return (
    <div className="shadow-lg p-3 mb-5 bg-white rounded">
        {/* <h1>{menuInfo.name}</h1>
        <img src={menuInfo.image} className="img-fluid" style={{height: '200px'}}></img> */}

        <div onClick={handleShow}>
          <h1>{menuInfo.name}</h1>
          <img src={menuInfo.image} className="img-fluid" style={{height: '200px'}}></img>
        </div>
        
        <div className="flex-container">

          <div className="w-100 m-1">
              <p>Order Type</p>
              <select className="form-control">{menuInfo.orderType.map(orderType =>{
                  return <option value={orderType}>{orderType}</option>
                })}
              </select>
          </div>

          <div className="w-100 m-1">
              <p>Quantity</p>
              <select className="form-control" value={quantity} onChange={(e)=>{setquantity(e.target.value)}}>
                  {[...Array(10).keys()].map((x, i)=>{
                    return <option value={i+1}>{i+1}</option>
                  })}
              </select>
          </div>

        </div>

        <div className="flex-container">
          
           <div className="m-1 w-100">
              <h1 className="mt-1">Price: {menuInfo.price * quantity}</h1>   
           </div>       

           <div className="m-1 w-100">
              <button className="btn" onClick={addtocart}>ADD TO CART</button>
           </div>    

        </div>
        
        <Modal show={show} onHandle={handleClose}>
          <Modal.Header closeButton onClick={handleClose}>
            <Modal.Title>{menuInfo.name}</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <img className="img-fluid" style={{height:"400px", width:"500px"}} src={menuInfo.image}/>
            <p>{menuInfo.description}</p>
          </Modal.Body>

          <Modal.Footer>
                <button className="btn" onClick={handleClose}>CLOSE</button>
          </Modal.Footer>
        </Modal>


    </div>
  )
}
