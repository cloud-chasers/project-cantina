import React, { useState, useEffect } from 'react'
import {useDispatch, useSelector} from 'react-redux'
import Loading from '../components/Loading'
import Error from '../components/Error'
import {getMenuItems, deleteMenu} from '../actions/menuActions'
import {BrowserRouter,Routes, Route ,Link , Switch} from 'react-router-dom'
import Editmenu from './Editmenu'



export default function Menulist(props){

    // if(props.update){
    //     console.log("props");
    //     props.update("Test string");
    // }
    const dispatch = useDispatch()

    const menusstate = useSelector(state=>state.getMenuItemsReducer)
  
    const {menus, error, loading} = menusstate

    useEffect(()=>{
        dispatch(getMenuItems())
      }, [])
  
    return (
      <div>
          <h2>Menu</h2>
          {loading && (<Loading/>)}
          {error && (<Error error="Something is not right"/>)}

          <table className="table table-bordered table-striped">
            <thead className="thead-dark">
                <tr>
                    <th>Name</th>
                    <th>Prices</th>
                    <th>Category</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {menus && menus.map(menu=>{
                return <tr>
                    <td> {menu.name}</td> 
                    <td> {menu.price}</td> 
                    <td> {menu.category}</td> 
                    <td> 
                        <i className="fa fa-trash m-1" onClick={()=>{dispatch(deleteMenu(menu._id))}}> </i>
                        <Link onClick={()=>{ /*console.log("Inside of onclick ")*/ /*+ typeof props.update)*/ props.update(menu._id);}} to={`/admin/editmenu/${menu._id}`}><i className="fa fa-edit m-1"> </i></Link>
                        {/* <Link to={'/admin/editmenu'}><i className="fa fa-edit m-1"> </i></Link> */}
                    </td> 
                    
                </tr>
            })}
            </tbody>
          </table>
      </div>
    )
  
}
