import logo from './logo.svg';
import './App.css';
import bootstrap from "../node_modules/bootstrap/dist/css/bootstrap.min.css"
import {BrowserRouter,Routes, Route ,Link , Switch} from 'react-router-dom'
import Navbar from './components/Navbar';
import Homescreen from './screens/Homescreen';
import Cartscreen from './screens/Cartscreen';
import Menuscreen from './screens/Menuscreen'
import Aboutscreen from './screens/Aboutscreen'
import Contactscreen from './screens/Contactscreen'

function App() {
  return (
    <div className="App">
      <Navbar/>
      <BrowserRouter>
        <Routes>
          {/* For different screens, add a file to the screens folder, add a route as shown below and update it in the Navbar page(add link) */}
          <Route path="/" exact  element={<Homescreen/>}/>
          <Route path="/cart" exact element ={<Cartscreen/>}/>
          <Route path="/menu" exact element ={<Menuscreen/>}/>
          <Route path="/about" exact element ={<Aboutscreen/>}/>
          <Route path="/contact" exact element ={<Contactscreen/>}/>
        </Routes>  
      </BrowserRouter>
      

    </div>
  );
}

export default App;
