# Project Cantina

## Overview
Yaquis Cantina brings to light the importance of the unique Yaquis culture in Mexico that is overridden by the exposure of several  popular mexican delicacies known 
globally. This unique and inclusive restaurant prioritizes quality and authentic food which is uncommon in the US and eliminates the stereotypical “fake” and 
fabricated Mexican food that is mimicked by several trendy restaurants that individuals tend to go to in order to satisfy their cravings. Along with the introduction of 
the obscure Yaquis culture, the team at Yaquis Cantina value the representation of minorities and continue to strive to exceed customer needs and supersede their 
accomplishments by adding new goals and cuisines to their menu. 

- What was our motivation?
    - We were motivated to create this website because we were inspired by Miguel's visions and wanted to show our support to the small business owner and help him achieve his dreams. During the pandemic a lot of businesses were shut down and because of this we were extra determined to help Miguel with this restaurant and support him by doing something amiable. 

- What problem is it solving?
    - By helping Miguel create this website for his new restaurant, we are saving him a lot time, money and effort that would be required of him to find someone capable of fulfilling his expectations. Our effort will also help him speed up the process of opening his restaurant. 

- What did we learn?
    - We as team collectively learned a lot this semester, because none of us have ever really developed a website before, there was a lot of self-learning that we needed to do on our part. However, we worked together to help each other whenever we experienced any issues. This semester we learned to be a better leader, team player, and most importantly we learned to be organized and stay on task by using Jira and completing each Sprints on time. We also received a lot of feedback from our professors who were our mentors this semester, as well as our client Miguel, who guided us to help produce a successful product.
    
- Impact on Yaquis Cantina:
    - An efficient way to track 
    customers and accept online 
    orders.
    - Ability to make online 
    reservations, this allows for 
    tracking the number of customers 
    that might be coming in for the 
    day
    - With these two pieces of data, we 
    can calculate metrics for the days 
    of the week.
    - The website itself can show 
    locations, latest news, and of 
    closures.
    - Easier way to keep track of 
    transactions and expenses.


## Table of Contents
- [Project Logo](#project logo)
- [Description](#description)
- [Visuals](#visuals)
- [Installation](#installation)
- [Usage](#usage)
- [Testing](#testing)
- [System Test Report](#system test report)
- [Maintenance Manual](#maintenance manual)
- [User Manual](#user manual)
- [Credits](#credits)
- [Support](#support)
- [Contributors](#contributors)

## Project Logo
![Logo](/Current%20Website%20Images/project_logo.png)

## Description
Our client needs software that will make it easier for their customers to understand all the services that our client can provide for them. The main features to be provided by the software are going to be brief descriptions of all the services provided at the client's restaurant. As per request of our client, specific menu items like Margarita rentals, taquiza, bartender services, catering services will be highlighted on the main page. This is a One Stop Shop and software should show that to customers that they can find everything they need to arrange any kind of event. There are many expected uses of the software, we expect the software to allow users to give feedback to the restaurant. We intend for users to scroll through menu items easily. Not only that but we intend to give the users a description of the food and restaurant. The description may include ingredients, allergy warnings, nutrition facts, etc. Description of the restaurant will be provided in the about page, which will help users contact the restaurant for questions and catering services if applicable and learn more about the history of its development. Another potential use will be the ability to order online, a login page for the administration side, and a database for storing relevant information.


## Visuals
The below images represent our finished product or website. The visuals provided below will give you an overview of each screen of our website.

Home Screen
![Home Screen](/Current%20Website%20Images/Home_Page_Phone_View.png)

Menu Screen
![Menu Screen](/Current%20Website%20Images/Menu_Screen.png)

About Screen
![About Screen](/Current%20Website%20Images/Home_Page_Web_View.png)

Cart Screen
![Cart Screen](/Current%20Website%20Images/Cart_Screen.png)

Contact Screen
![Contact Screen](/Current%20Website%20Images/Contact_Screen.png)

Login Screen
![Login Screen](/Current%20Website%20Images/Login_Screen.png)

Register Screen
![Register Screen](/Current%20Website%20Images/Register_Screen.png)

## Installation
There is no need to install this as it is a web based application. It can be accessed through any devices that have internet capabilities. We suggest using the default website browser, whether that is Firefox or Google Chrome, it will run on both.

## Usage
As per requirements, the user can navigate from one screen to another on Home Screen, able to choose multiple items from the menu, add it to the cart, and place an order successfully. To place an order, the user can register on the Register screen and then login with the registered credentials for future use as well.

## Testing
Please refer to the attached link for test scripts:
[Tests/test scripts](https://gitlab.com/cloud-chasers/project-cantina/-/tree/main/Tests)

## System Test Report
Please refer to the attached link for how to run tests in Firefox and Google Chrome: 
[System Test Report](https://docs.google.com/document/d/1Q0Sxxv32BhLAYBZ7RFgZ5kWVY3aiJtYj/edit?usp=sharing&ouid=114409556311673116567&rtpof=true&sd=true)

## Maintenance Manual
Please refer to the attached link for how to run the project, resolve issues, or anything related to the project, etc.:
[Maintenance Manual](https://docs.google.com/document/d/13Ni9bpOTHxZpnHQ0yw4-cP2knEP6hSAA/edit?usp=sharing&ouid=114409556311673116567&rtpof=true&sd=true)

## User Manual
Please refer to the attached link for how to start using the website or learn about all the core aspects of website in detail:
[User Manual](https://docs.google.com/document/d/10cAs0ST3tV1ZiS1QdTwlgZ7z014COstipumDonDoYC4/edit?usp=sharing)

## Credits
The credits go to Professor Kenneth Elliot, Client Miguel Canez, and Lab Advisor Professor Chen and Professor Jin.

## Support
If need clarification related to the functionality or issues about website, please refer to the attached documents such as Software Test Report, User Manual, and Maintenance Manual. 

## Contributors
Our team is composed of 8 members:

Brian Bhatti, brianbhatti@csus.edu

Pronisha Panta, pronishapanta@csus.edu

Jasmeen Sahota, ujasmeen@csus.edu

Nestor Preciado, nestorpreciado@csus.edu

Paneri Patel, paneripatel@csus.edu

Ahmed Abduljabbar, aabduljabbar@csus.edu

Kuljit Dosanjh, kdosanjh@csus.edu

Gurdeep Gill, gurdeepgill@csus.edu

