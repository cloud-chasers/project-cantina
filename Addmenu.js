import React, {useState, useEffect} from 'react'
import {useDispatch,useSelector} from 'react-redux'
import {addMenu} from "../actions/menuActions"
import Loading from '../components/Loading'
import Error from '../components/Error'
import Success from '../components/Success'

export default function Addmenu() {

  const[name, setname] = useState('')
  const[price, setprice] = useState()
  const[image, setimage] = useState('')
  const[description, setdescription] = useState('')
  const[category, setcategory] = useState('')

  const dispatch = useDispatch()

  const addmenusstate = useSelector(state=>state.addMenuItemsReducer)
  const {success , error , loading} = addmenusstate
  function formHandler(e){

    e.preventDefault();

    const menu ={
        name ,
        image,
        description,
        category,
        price
    }
    if(name!=='' && price!=='' && image!=='' && description!=='' && category!=='') {
      console.log(menu);
      dispatch(addMenu(menu));
    }
    else{
      alert("Make sure all fields are filled out!")
    }

  }

  return (
    <div>
        
        <div className="text-left">
          <h1> Add Menu</h1>
          {loading && (<Loading/>)}
          {error && (<Error error='Something went wrong'/>)}
          {success && (<Success success='Menu Item added successfully'/>)}
          <form onSubmit={formHandler}>
            <input className="form-control" type="text" placeholder="name" value={name} onChange={(e) => { setname(e.target.value);}}/>
            <input className="form-control" type="text" placeholder="price" value={price} onChange={(e) => { setprice(e.target.value);}}/>

            <input className="form-control" type="text" placeholder="image url" value={image} onChange={(e) => { setimage(e.target.value);}}/>
            <input className="form-control" type="text" placeholder="description" description={price} onChange={(e) => { setdescription(e.target.value);}}/>
            <input className="form-control" type="text" placeholder="category" value={category} onChange={(e) => { setcategory(e.target.value);}}/>
            <button className="btn mt-3" type="submit"> Add Menu Item </button>
          </form>

        </div>

    </div>
  )
}
