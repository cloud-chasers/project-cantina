import React from 'react'
import styled from "styled-components";

export default function Aboutscreen() {
  return (
  <Wrapper>
    <div class ="container">
  <div class = "bg">
    <img src ="./images/cantina.jpg"/>
  </div>
    <div class = "text">
      <h2>About Us</h2>
      <p>Yaquis Cantina is a restaurant founded by Miguel Canes Jr.
           who was inspired by the Indigenous people of Mexico known
           as "the Yaqui." The restaurant features a variety of mouth
           watering Mexican delicacies as well as unique and special
           alcoholic drinks which aim to provide the authentic Mexican
           restaurant experience. 
           </p>
      <p>We value inclusiveness in the community as well as representing
         the minorities with our unique and delicious take on authentic Mexican cuisines that introduces
        the world of the important elements of the Yaqui communities.</p>
      
    </div>
    <div class="footer">    
    <h1><a href="mailto: ycantina@gmail.com">Email us: ycantina@gmail.com </a></h1>
    <a href="https://goo.gl/maps/raDdfBiXm4bvu63D7">Location: 703 Main St, Woodland CA, 95695</a>
       
   </div>   
</div>
</Wrapper>
);
}

const Wrapper = styled.section`


.bg{
   position: fixed;
    height: 100%; 
    width: 50%;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    left: 0px;
}

.container{
    resize: none;
  }

.text{
  padding-top: 2%;
  padding-bottom: 2%;
  padding-right: 2%;
  padding-left: 2%;
  background-color: black;
  color: white;
  font-weight: 900;
  font-size: 20px;
   width: 50%;
   position: fixed;
   right: 0px;
   height: 100%;

   p {
   
    font-size: 20px;
    color: white;
    font-weight: 700;
    
    
  }
  h2 {
    text-transform: capitalize;
    color: white;
    font-weight: 900;
    padding-top: 12%;
    font-size: 30px;
  }
}

img{
  max-width: 100%;
  height: 100%;
}

.footer{

  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  height: 6%;
  background-color: gainsboro;
 
  text-align: center;
  padding:0.2%;
  

a{
  
    font-weight: 700;
    float: right;
    font-size: 20px;
    padding-right: 1%;
    padding-top: 0.4%;
}
  
h1 {
    a{
  
      font-weight: 700;
      float: left;
      font-size: 20px;
      padding-left: 1%;
      padding-top: 1%;
  }
   
  }
  

  h2 {
    text-transform: capitalize;
    font-weight: 700;
    color: black;
    float: center;
    font-size: 20px;
    padding-top: 0.6%;
  }
}
`;
 