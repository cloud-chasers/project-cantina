// import React from 'react'
import React, { useRef } from 'react';
import emailjs from '@emailjs/browser';
import styled from "styled-components";



export default function Contactscreen() {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();
    console.log(e)
    if(e.target[0].value !== "" && e.target[1].value !== "" && e.target[2].value !== ""){
    emailjs.sendForm('service_9r4ox2m', 'template_5c7xcpd', form.current, 'T9utwrLj1YAG4ueCw')
      .then((result) => {
          console.log(result.text);
          console.log("Message sent successfully");
          e.target.reset();
      }, (error) => {
          console.log(error.text);
      });
    }
    else{
      alert("Make sure all fields are filled out!")
    }
  };
  return (
    <div className="contact-us">
      <div className="row justify-content-center mt-5">
          <div className="col-md-7 mt-5 text-left shadow-lg p-3 mb-5 bg-white rounded">
          <h2 className="text-center m-2" style={{ fontSize: "35px" }}>
            Contact Us
          </h2>
          <div>
          <form ref={form} onSubmit={sendEmail}>
            <input className="form-control" placeholder="Name" type="text" name="user_name" />
            <input className="form-control" placeholder="Email" type="email" name="user_email" />
            <br/>
            <textarea className="form-control" name="message" placeholder="Message" />
            <br/>
            <input className="form-control" className="btn mt-3 mb-3" type="submit" value="Send" />
          </form>
          </div>
        </div>
      </div>
    </div>
  )
}
