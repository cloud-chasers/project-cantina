const express = require("express");
const menu = require('./models/menuModel')

const app = express();
const db = require("./db.js")

app.use(express.json());

const menuRoute = require('./routes/menuRoute')
const userRoute = require('./routes/userRoute')
const orderRoute = require('./routes/orderRoute')

app.use('/api/menus/', menuRoute)
app.use('/api/users/', userRoute)
app.use('/api/orders/', orderRoute)

app.get("/", (req, res) => {
    res.send("server working");
});



// app.get("/api/menus/getmenus", (req, res) => {
//     console.log("Hello")
//     res.send("Hello")
// })

// app.get("/getmenus", (req, res) =>{
//     menu.find({}, (err , docs)=>{
//         if(err){
//             console.log(err)
//         }
//         else{
//             res.send(docs);
            
//         }
//     })

// });




const port = process.env.PORT || 9000;

app.listen(port, () => 
    "Working"
);
