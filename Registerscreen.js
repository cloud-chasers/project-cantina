import React, { useState, useEffect } from "react";
import {useDispatch , useSelector} from 'react-redux'
import { registerUser } from "../actions/userActions";
import Error from "../components/Error";
import Loading from "../components/Loading";
import Success from '../components/Success'
export default function Registerscreen() {
  const [name, setname] = useState("");
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");
  const [cpassword, setcpassword] = useState("");
  const registerstate = useSelector(state =>state.registerUserReducer)
  const {loading , error , success} = registerstate
  const dispatch = useDispatch()
  function register(){

      if(password!=cpassword)
      {
          alert("passwords not matched")
      }
      else{
          const user={
              name,
              email,
              password
          }
          if(name !== "" && email !== "" && password !== ""){
            console.log(user);

            // Lines 31-42 are added to check password requirements
            var minNumberofCharacters = 8;
            var maxNumberofCharacters = 21;
            var regexp = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/;
            const regexpemail = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            
            if(!regexp.test(password)) 
            {
              // if(password.length < minNumberofCharacters || password.length > maxNumberofCharacters)
                console.log("inside of register screen")
                alert("Make sure password is greater than 8 characters and less than 20 characters and contains at least one number and at least one special character")
            }
            if(!regexpemail.test(email))
            {
              alert("Enter valid a email address")
            }
            if(regexpemail.test(email) && regexp.test(password) )
            {
              console.log("inside of register screen")
              dispatch(registerUser(user))
            }


            // else{
            //   console.log("inside else")
            //   dispatch(registerUser(user))
            // }

            // dispatch(registerUser(user))
          }
          else{
            alert("Make sure all fields are filled out!")
          }
      }

  }

  return (
    <div className='register'>
      <div className="row justify-content-center mt-5">
        <div className="col-md-5 mt-5 text-left shadow-lg p-3 mb-5 bg-white rounded">

          {loading && (<Loading/>)}
          {success && (<Success success='User Registered Successfully' />)}
          {error && (<Error error='Email already registered' />)}

          <h2 className="text-center m-2" style={{ fontSize: "35px" }}>
            Register
          </h2>
          <div>
            <input id="register_name" required type="text" placeholder="name" className="form-control" value={name} onChange={(e)=>{setname(e.target.value)}} />
            <input id="register_email" required type="text" placeholder="email" className="form-control" value={email} onChange={(e)=>{setemail(e.target.value)}} />
            <input
              id="register_password"
              type="password"
              placeholder="password"
              className="form-control"
              value={password}
              required
              onChange={(e)=>{setpassword(e.target.value)}}
            />
            <input
              id="register_confirmpassword"
              type="password"
              placeholder="confirm password"
              className="form-control"
              value={cpassword}
              required
              onChange={(e)=>{setcpassword(e.target.value)}}
            />
            <button id="register_btn" onClick={register} className="btn mt-3 mb-3">REGISTER</button>
            <br/>
            <a id="login_screen" style={{color:'black'}} href="/login">Click Here To Login</a>
          </div>
        </div>
      </div>
    </div>
  );
}