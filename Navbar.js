import React from 'react'
import {useSelector, useDispatch} from 'react-redux'
import { logoutUser } from "../actions/userActions";
import {DropdownButton} from 'react-bootstrap'

export default function Navbar() {
  const cartstate = useSelector(state=>state.cartReducer)
  const userstate = useSelector((state) => state.loginUserReducer);
  const { currentUser } = userstate;
  const dispatch = useDispatch()
  return (
    <nav className="navbar navbar-expand-lg shadow-lg p-3 mb-5 bg-white rounded">
        <a className="navbar-brand" href="/">Yaquis Cantina</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"><i style={{color:'black'}} className="fas fa-bars"></i></span>
        </button>
        <div className="collapse navbar-collapse justify-content-around" id="navbarNav">
            <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                <a className="nav-link" href="/">Home</a>
                </li>
                <li className="nav-item">
                <a className="nav-link" href="/menu">Menu</a>
                </li>
                <li className="nav-item">
                <a className="nav-link" href="/about">About</a>
                </li>
                <li className="nav-item">
                <a className="nav-link" href="/contact">Contact</a>
                </li>
                <li className="nav-item">
                <a className="nav-link" href="/cart">Cart {cartstate.cartItems.length}</a>
                </li>

                {currentUser ? (
                  <DropdownButton title= {currentUser.name}>
                      <div>
                        <div>
                          <a href="/orders">Orders</a>
                        </div>
                        <div>
                          <a href="#" onClick={()=>{dispatch(logoutUser())}}><li>Logout</li></a>
                        </div>   
                      </div>
                  </DropdownButton>
                  ) : (
                  <li className="nav-item">
                    <a className="nav-link" href="/login">Login</a>
                  </li>)}
            </ul>
        </div>
    </nav>
  )
}
