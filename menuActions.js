import axios from 'axios';
export const getMenuItems = () => async dispatch=>{
    dispatch({type: 'GET_MENU_REQUEST'})

    try{
        
        const response = await axios.get('/api/menus/getallmenus')
        
        console.log(response)
        dispatch({type: 'GET_MENU_SUCCESS', payload: response.data})
    }catch(error){
        dispatch({type: 'GET_MENU_FAILED', payload: error})
    }


}

export const addMenu=(menu)=>async dispatch=>{
    dispatch({type: 'ADD_MENU_REQUEST'})
    try {

        const response = await axios.post('/api/menus/addmenu', {menu})
        console.log(response)
        dispatch({type: 'ADD_MENU_SUCCESS'})
        
    } catch (error) {

        dispatch({type: 'ADD_MENU_FAILED', payload: error})
        
    }
}



export const getMenuByID = (menuid) => async dispatch=>{
    dispatch({type: 'GET_MENUBYID_REQUEST'})

    try{
        
        const response = await axios.post('/api/menus/getmenubyid', {menuid})
        
        console.log(response)
        dispatch({type: 'GET_MENUBYID_SUCCESS', payload: response.data})
    }catch(error){
        dispatch({type: 'GET_MENUBYID_FAILED', payload: error})
    }


}


export const editMenu=(editedmenu)=>async dispatch=>{
    dispatch({type: 'EDIT_MENU_REQUEST'})
    try {

        const response = await axios.post('/api/menus/editmenu', {editedmenu})
        console.log(response)
        dispatch({type: 'EDIT_MENU_SUCCESS'})
        window.location.href ='/admin/menulist'
        
    } catch (error) {

        dispatch({type: 'EDIT_MENU_FAILED', payload: error})
        
    }
}

export const deleteMenu=(menuid)=>async dispatch=>{
    try {
        const response = await axios.post('/api/menus/deletemenu', {menuid})
        alert("Item deleted successfully")
        console.log(response)
        window.location.reload()
    } catch (error) {
        alert("Error deleting item")
        console.log(error)
    }
}

export const getAllUsers = () => async dispatch=>{
    dispatch({type: 'GET_USERS_REQUEST'})

    try{
        const response = await axios.get('/api/menus/getallusers')
        
        console.log(response)
        dispatch({type: 'GET_USERS_SUCCESS', payload: response.data})
    }catch(error){
        dispatch({type: 'GET_USERS_FAILED', payload: error})
    }


}

export const filterMenu=(searchkey , category)=>async dispatch=>{

  
    dispatch({type:'GET_MENU_REQUEST'})

    try {
        var filterMenu;
        const response = await axios.get('/api/menus/getallmenus')
        filterMenu = response.data.filter(menu=>menu.name.toLowerCase().includes(searchkey.toLowerCase()))
         
        if(category!=='all')
        {
            console.log(category)
            filterMenu = response.data.filter(menu=>menu.category.toLowerCase()===category.toLowerCase())

        }
        dispatch({type:'GET_MENU_SUCCESS' , payload : filterMenu})
    } catch (error) {
        dispatch({type:'GET_MENU_FAILED' , payload : error})
    }

}
