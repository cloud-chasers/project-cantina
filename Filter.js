import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { filterMenu } from "../actions/menuActions";
export default function Filter() {
    const dispatch = useDispatch()
    const[searchkey , setsearchkey] = useState('')
    const[category , setcategory] = useState('all')
    return (
        <div className='container col-md-6 offset-md-3'>
            <div style={{height: '10vh', justifyContent: 'center', alignItems: 'center', width:'100%'}} className="row justify-content-center shadow-lg p-2 mb-5 bg-white rounded">

                {/* <div className="col-md-1 w-100">
                      <input
                      onChange={(e)=>{setsearchkey(e.target.value)}}
                      value={searchkey} type="text" className="form-control w-100" placeholder="Search Menu"/>
                    input-group -> class
                    </div> */}
                    <div className="col-md-1 w-100">
                       <select className="form-control form-select w-50 mt-1 d-inline" value={category} onChange={(e)=>setcategory(e.target.value)}>
                            <option id="all" value="all">All</option>
                            <option id="extras" value="Extras">Extras</option>
                            <option id="tacos" value="Taco">Tacos</option>
                            <option id="beers" value="Beer">Beers</option>
                            <option id="quesadilla" value="Quesadilla">Quesadilla</option>
                            <option id="rice"value="Rice">Rice</option>
                            <option id="beans" value="Beans">Beans</option>
                        </select>
                        <button style={{height: '40px', marginBottom:'4px'}} className='btn btn-outline-secondary' onClick={()=>{dispatch(filterMenu(searchkey , category))}}>FILTER</button>
                    </div>

                    
                    {/* <div className="col-md-3 w-100">
                       <button className='btn w-100 mt-1' onClick={()=>{dispatch(filterMenu(searchkey , category))}}>FILTER</button>
                    </div> */}


            </div>
        </div>
    )
}