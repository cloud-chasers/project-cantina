import React from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {addToCart} from '../actions/cartActions'
import {deleteFromCart} from '../actions/cartActions'
import Checkout from '../components/Checkout'

export default function Cartscreen() {

  const cartstate = useSelector(state=>state.cartReducer)
  const cartItems = cartstate.cartItems
  var subTotal = cartItems.reduce((x, item)=>x+item.totalPrice, 0)
  const dispatch = useDispatch()
  return (

    <div> 
        
        <div className="row justify-content-center">

            <div className=".col-md-6">
                <h2 style={{fontSize:'40px', paddingRight: "85%"}}>My Cart<i style={{fontSize:'30px', marginLeft:'10px'}}class="fa fa-shopping-cart" aria-hidden="true"></i></h2>

                {cartItems.map(item=>{
                    return <div className="flex-container">
                        <div className="w-100 text-left">
                            <h1> {item.name} </h1>
                            <h1> Price : {item.quantity} * {item.price} = {item.price * item.quantity} </h1>
                            <h1 style={{display:'inline'}}> Quantity : </h1>
                            <i className="fa fa-plus" aria-hidden="true" onClick={()=>{dispatch(addToCart(item,item.quantity+1))}}></i>
                            <b>{item.quantity}</b>
                            
                            <i className="fa fa-minus" aria-hidden="true" onClick={()=>{dispatch(addToCart(item,item.quantity-1))}}></i>
                            <hr/>
                        </div>

                        <div className='m-1 w-100'>
                            <img src={item.image} style={{height:'100px', width: '150px'}}/>
                        </div>

                        <div className='m-1 w-100'>
                        <i className="fa fa-trash mt-5" aria-hidden="true" onClick={()=>{dispatch(deleteFromCart(item))}}></i>
                        </div>

                    </div>
                })}



            </div>

            <div className=".col-md-4">
            <h2 style={{fontSize:'45px',textAlign:'left', paddingLeft: '70%'}}>Total: ${subTotal}</h2>
            <Checkout subtotal={subTotal} />

            </div>

        </div>
    
    </div>
  )
}
