const db = require('mongoose') 
require('dotenv').config();

let mongod = undefined;

describe('database testing', () => {

    const menu = db.model("test_"+process.env.COLLECTION+"s",db.Schema({
        name: String,
        price: String
    }));


    
    beforeAll(async () => {
        mongod = "mongodb+srv://brianbhatti:GodFwzeMVyNTekIO@cluster0.4zfqp.mongodb.net/project-cantina"
        db.connect(mongod, {useUnifiedTopology:true, useNewurlParser:true}) 
    });

    afterAll(async () => {
        const collection = process.env.COLLECTION;
        await db.connection.dropCollection("test_"+process.env.COLLECTION+"s");
        await db.connection.close();
    });

    test("Add food item",async () => {
        
        const response = await menu.create({
            name: process.env.FOOD_NAME2,
            price: process.env.FOOD_PRICE2
        });
        
        expect(response.name).toBe(process.env.FOOD_NAME2);
        expect(response.price).toBe(process.env.FOOD_PRICE2);
    });

    jest.setTimeout(30000);
    test("Add 100 food item",async () => {
        
        let response = {};
        for(let i = 0; i< 100; i++){
            response[i] = await menu.create({
                name: process.env.FOOD_NAME,
                price: process.env.FOOD_PRICE
            });
            await response[i].save();
            expect(response[i].name).toBe(process.env.FOOD_NAME);
            expect(response[i].price).toBe(process.env.FOOD_PRICE);
        }
    });


    test("All menu", async () => {

        const response = await menu.find({});
        expect(response.length).toBeGreaterThan(100);

    });

    test("Update food price", async () => {
  
        const response = await menu.updateOne({name: process.env.FOOD_NAME},{price: process.env.FOOD_PRICE_ALT});
        expect(response).toBeTruthy();

    });

    test("Food price updated", async () => {

        const responseTwo = await menu.findOne({name: process.env.FOOD_NAME});
        expect(responseTwo.price).toBe(process.env.FOOD_PRICE_ALT);
        expect(responseTwo.name).toBe(process.env.FOOD_NAME);

    });

    test("Delete food item", async() => {
    
        const response = await menu.deleteOne({name: process.env.FOOD_NAME});
        expect(response.deletedCount).toBe(1);
  

    });
    

  
});