# Menu item through admin portal, item is then verfied on menu page, deleted and then verified again
from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.alert import Alert
PATH = "/Users/brianbhatti/Desktop/Testing/chromedriver"

#from webdriver_manager.chrome import ChromeDriverManager
#from selenium.webdriver.chrome.service import Service
#driver = webdriver.Chrome(service=Service(executable_path=ChromeDriverManager().install()))

driver = webdriver.Chrome(PATH)
driver.get("http://localhost:3000/login")
driver.maximize_window()
time.sleep(2)

log_in = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/nav[1]/div[1]/ul[1]/li[6]/a[1]")))
log_in.click()

username_search =  driver.find_element(By.ID, 'email_login')
password_search =  driver.find_element(By.ID, 'password_login')
login_btn     =  driver.find_element(By.ID, 'login_click')

time.sleep(2)
username_search.send_keys("brianbhattihp@gmail.com")
time.sleep(2)
password_search.send_keys("India9157190")
time.sleep(2)

login_btn.click()

time.sleep(2)
driver.get("http://localhost:3000/admin/addmenu")

alert = Alert(driver)

name = driver.find_element(By.ID, 'addmenu_name')
price = driver.find_element(By.ID, 'addmenu_price')
image = driver.find_element(By.ID, 'addmenu_imageurl')
description = driver.find_element(By.ID, 'addmenu_description')
category = driver.find_element(By.ID, 'addmenu_category')
add_menu_btn = driver.find_element(By.ID, 'addmenu_button')

time.sleep(1)
name.send_keys("Test item 1")
time.sleep(1)
price.send_keys("20")
time.sleep(1)
image.send_keys("https://images.immediate.co.uk/production/volatile/sites/30/2020/08/chorizo-mozarella-gnocchi-bake-cropped-9ab73a3.jpg?quality=90&resize=768,574")
time.sleep(1)
description.send_keys("This is a test item for our menu")
time.sleep(1)
category.send_keys("Test Category")
time.sleep(1)
add_menu_btn.click()
time.sleep(1)

driver.get("http://localhost:3000/menu")
time.sleep(2)
driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
time.sleep(2)
driver.get("http://localhost:3000/admin/menulist")
time.sleep(2)
driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
time.sleep(2)

delete_menu_item = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//tbody/tr[19]/td[4]/i[1]")))

delete_menu_item.click()
time.sleep(2)
alert.accept()

time.sleep(2)
driver.get("http://localhost:3000/menu")
time.sleep(2)
driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
time.sleep(2)

driver.quit()
