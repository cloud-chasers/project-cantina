from selenium import webdriver
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.alert import Alert
PATH = "/Users/brianbhatti/Desktop/Testing/chromedriver"

driver = webdriver.Chrome(PATH)
driver.get("http://localhost:3000/contact")
driver.maximize_window()
time.sleep(2)

name = driver.find_element(By.ID, 'contact_name')
email = driver.find_element(By.ID, 'contact_email')
message = driver.find_element(By.ID, 'contact_message')
contact_btn = driver.find_element(By.ID, 'contact_btn')

alert = Alert(driver)

# Testing with an empty field
time.sleep(1)
name.send_keys("Pronisha Panta")
time.sleep(1)
email.send_keys("pantapronisha@gmail.com")
time.sleep(1)
contact_btn.click()
time.sleep(1)
alert.accept()

name.clear()
name.send_keys("Pronisha Panta")
time.sleep(1)
email.clear()
email.send_keys("pantapronisha@gmail.com")
time.sleep(1)
message.clear()
message.send_keys("Hi my name is Pronisha Panta and I love pumpkin spice lattes")
time.sleep(1)

contact_btn.click()
time.sleep(1)
driver.quit()
