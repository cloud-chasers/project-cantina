# Tests registering a new user, logging in as admin and deleteing that user account from the admin panel
from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.alert import Alert
PATH = "/Users/pronishapanta/Desktop/Testing/chromedriver"

driver = webdriver.Chrome(PATH)
driver.get("http://localhost:3000/register")
driver.maximize_window()
time.sleep(2)

name = driver.find_element(By.ID, 'register_name')
email = driver.find_element(By.ID,'register_email')
password = driver.find_element(By.ID, 'register_password')
confirm_password = driver.find_element(By.ID, 'register_confirmpassword')

register_btn = driver.find_element(By.ID, 'register_btn')

time.sleep(1)
name.send_keys("Jane Doe")
time.sleep(1)
email.send_keys("Jane_Doe@yahoo.com")
time.sleep(1)
password.send_keys("password@113")
time.sleep(1)
confirm_password.send_keys("password@113")
time.sleep(1)
register_btn.click()
time.sleep(1)


driver.get("http://localhost:3000/login")

#try:
#    logged_in = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/nav[1]/div[1]/ul[1]/div[1]/button[1]")))
#except:
#    print("User is logged in")


log_in = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/nav[1]/div[1]/ul[1]/li[6]/a[1]")))
log_in.click()

username_search =  driver.find_element(By.ID, 'email_login')
password_search =  driver.find_element(By.ID, 'password_login')
login_btn     =  driver.find_element(By.ID, 'login_click')


time.sleep(2)
username_search.send_keys("Jane_Doe@yahoo.com")
time.sleep(2)
password_search.send_keys("password@113")
time.sleep(2)

login_btn.click()

logout_user_test = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/nav[1]/div[1]/ul[1]/div[1]/button[1]")))
logout_user_test.click()
time.sleep(2)
logout_user_test = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/nav[1]/div[1]/ul[1]/div[1]/div[1]/div[1]/div[2]/a[1]")))

driver.execute_script("arguments[0].click();", logout_user_test)
time.sleep(2)

username_search =  driver.find_element(By.ID, 'email_login')
password_search =  driver.find_element(By.ID, 'password_login')
login_btn     =  driver.find_element(By.ID, 'login_click')

time.sleep(2)
username_search.send_keys("brianbhattihp@gmail.com")
time.sleep(2)
password_search.send_keys("India9157190")
time.sleep(2)

login_btn.click()

time.sleep(2)
driver.get("http://localhost:3000/admin")

alert = Alert(driver)

delete_user = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//tbody/tr[6]/td[4]/i[1]")))

time.sleep(2)
delete_user.click()
time.sleep(2)
alert.accept()
time.sleep(2)

logout_user = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/nav[1]/div[1]/ul[1]/div[1]/button[1]")))
logout_user.click()
time.sleep(2)

logout_user_two = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/nav[1]/div[1]/ul[1]/div[1]/div[1]/div[1]/div[2]/a[1]")))

driver.execute_script("arguments[0].click();", logout_user_two)
#logout_user_two.click()
time.sleep(2)

username_search =  driver.find_element(By.ID, 'email_login')
password_search =  driver.find_element(By.ID, 'password_login')
login_btn     =  driver.find_element(By.ID, 'login_click')

time.sleep(2)
username_search.send_keys("Jane_Doe@yahoo.com")
time.sleep(2)
password_search.send_keys("password@113")
time.sleep(2)

login_btn.click()
time.sleep(2)

driver.close()
