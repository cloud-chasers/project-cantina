from selenium import webdriver
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.alert import Alert
PATH = "/Users/nestor/Desktop/Testing/chromedriver"

driver = webdriver.Chrome(PATH)
driver.get("http://localhost:3000/register")
driver.maximize_window()
time.sleep(2)

name = driver.find_element(By.ID, 'register_name')
email = driver.find_element(By.ID,'register_email')
password = driver.find_element(By.ID, 'register_password')
confirm_password = driver.find_element(By.ID, 'register_confirmpassword')

register_btn = driver.find_element(By.ID, 'register_btn')

alert = Alert(driver)

# This test case should result in two alert messages, email and password.
time.sleep(1)
name.send_keys("Jane Doe")
time.sleep(1)
email.send_keys("Jane_Doe@yahoo.c")
time.sleep(1)
password.send_keys("password")
time.sleep(1)
confirm_password.send_keys("password")
time.sleep(1)
register_btn.click()
time.sleep(1)
alert.accept()
time.sleep(1)
alert.accept()
time.sleep(1)

# Testing the integration of email and password validation, with correct email entered.
# Email is validated by a regex, there can be a service used to check for a valid email
# Due to time constraints we are sticking with the regex
name.clear()
time.sleep(1)
name.send_keys("Jane Doe")
time.sleep(1)
email.clear()
time.sleep(1)
email.send_keys("Jane_Doe@yahoo.com")
time.sleep(1)
password.clear()
time.sleep(1)
password.send_keys("password")
time.sleep(1)
confirm_password.clear()
time.sleep(1)
confirm_password.send_keys("password")
time.sleep(1)
register_btn.click()
time.sleep(1)
alert.accept()
time.sleep(1)

# Testing the integration of email and password validation, with correct password entered.
# Password is validated through a regex
name.clear()
time.sleep(1)
name.send_keys("Jane Doe")
time.sleep(1)
email.clear()
time.sleep(1)
email.send_keys("Jane_Doe@yahoo.c")
time.sleep(1)
password.clear()
time.sleep(1)
password.send_keys("password@113")
time.sleep(1)
confirm_password.clear()
time.sleep(1)
confirm_password.send_keys("password@113")
time.sleep(1)
register_btn.click()
time.sleep(1)
alert.accept()
time.sleep(1)

# Testing a successful user login making sure all fields work together correctly
name.clear()
time.sleep(1)
name.send_keys("Jane Doe")
time.sleep(1)
email.clear()
time.sleep(1)
email.send_keys("Jane_Doe@yahoo.com")
time.sleep(1)
password.clear()
time.sleep(1)
password.send_keys("password@113")
time.sleep(1)
confirm_password.clear()
time.sleep(1)
confirm_password.send_keys("password@113")
time.sleep(1)
register_btn.click()
time.sleep(1)

# Testing to register the same user again
name.clear()
time.sleep(1)
name.send_keys("Jane Doe")
time.sleep(1)
email.clear()
time.sleep(1)
email.send_keys("Jane_Doe@yahoo.com")
time.sleep(1)
password.clear()
time.sleep(1)
password.send_keys("password@113")
time.sleep(1)
confirm_password.clear()
time.sleep(1)
confirm_password.send_keys("password@113")
time.sleep(1)
register_btn.click()
time.sleep(1)

# Try logging in with the new user
login_link = driver.find_element(By.ID, 'login_screen')
driver.maximize_window()
time.sleep(2)
login_link.click()
username_search =  driver.find_element(By.ID, 'email_login')
password_search =  driver.find_element(By.ID, 'password_login')
login_btn       =  driver.find_element(By.ID, 'login_click')

time.sleep(1)
username_search.send_keys("Jane_Doe@yahoo.com")
time.sleep(1)
password_search.send_keys("password@113")
time.sleep(1)

login_btn.click()
time.sleep(1)


driver.quit()

