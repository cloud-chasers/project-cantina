from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.alert import Alert
PATH = "/Users/Jasmeen/Desktop/Testing/chromedriver"

driver = webdriver.Chrome(PATH)
driver.get("http://localhost:3000/menu")
driver.maximize_window()
time.sleep(2)

filter = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(),'FILTER')]")))

dropdown = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[1]/select[1]")))
dropdown.click()
time.sleep(2)

all = driver.find_element(By.ID, 'all')
all.click()
filter.click()

driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
time.sleep(2)
driver.execute_script("window.scrollTo(document.body.scrollHeight,0)")
time.sleep(2)

dropdown.click()
time.sleep(2)

extras = driver.find_element(By.ID, 'extras')
extras.click()
time.sleep(2)
filter.click()

driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
time.sleep(2)
driver.execute_script("window.scrollTo(document.body.scrollHeight,0)")
time.sleep(2)

dropdown.click()
time.sleep(2)

tacos = driver.find_element(By.ID, 'tacos')
tacos.click()
filter.click()

driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
time.sleep(2)
driver.execute_script("window.scrollTo(document.body.scrollHeight,0)")
time.sleep(2)

dropdown.click()
time.sleep(2)

beers = driver.find_element(By.ID, 'beers')
beers.click()
filter.click()
time.sleep(2)

dropdown.click()
time.sleep(2)

quesadilla = driver.find_element(By.ID, 'quesadilla')
quesadilla.click()
filter.click()
time.sleep(2)

dropdown.click()
time.sleep(2)

rice = driver.find_element(By.ID, 'rice')
rice.click()
filter.click()
time.sleep(2)

dropdown.click()
time.sleep(2)

beans = driver.find_element(By.ID, 'beans')
beans.click()
filter.click()
time.sleep(2)

driver.quit()









