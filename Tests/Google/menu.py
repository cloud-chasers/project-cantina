# Tests adding items to cart and then incrementing/decrementing on cart page and deleting of items
from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.alert import Alert
PATH = "/Users/brianbhatti/Desktop/Testing/chromedriver"

driver = webdriver.Chrome(PATH)
driver.get("http://localhost:3000/menu")

# Relative XPATH is used

add_to_cart_item_one = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[3]/div[2]/button[1]")))

add_to_cart_item_one.click()

time.sleep(1)

add_to_cart_item_two = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[3]/div[2]/button[1]")))

add_to_cart_item_two.click()

time.sleep(1)

add_to_cart_item_three = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[3]/div[2]/button[1]")))

add_to_cart_item_three.click()

time.sleep(1)

add_to_cart_item_four = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[2]/div[6]/div[1]/div[1]/div[3]/div[2]/button[1]")))

time.sleep(1)

cart = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//a[@id='cart']")))

cart.click()

increment = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/i[1]")))

increment.click()
time.sleep(1)
increment.click()
time.sleep(1)
increment.click()
time.sleep(1)
increment.click()
time.sleep(1)
increment.click()
time.sleep(1)
increment.click()
time.sleep(1)
increment.click()
time.sleep(1)
increment.click()
time.sleep(1)
increment.click()
time.sleep(1)
increment.click()
time.sleep(1)

alert = Alert(driver)

alert.accept()


increment_two = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/i[1]")))

increment_two.click()
time.sleep(1)
increment_two.click()
time.sleep(1)
increment_two.click()
time.sleep(1)
increment_two.click()
time.sleep(1)
increment_two.click()
time.sleep(1)
increment_two.click()
time.sleep(1)
increment_two.click()
time.sleep(1)
increment_two.click()
time.sleep(1)
increment_two.click()
time.sleep(1)
increment_two.click()
time.sleep(1)

alert.accept()

decrement_two = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/i[2]")))

decrement_two.click()
time.sleep(1)
decrement_two.click()
time.sleep(1)
decrement_two.click()
time.sleep(1)
decrement_two.click()
time.sleep(1)
decrement_two.click()
time.sleep(1)
decrement_two.click()
time.sleep(1)
decrement_two.click()
time.sleep(1)
decrement_two.click()
time.sleep(1)
decrement_two.click()
time.sleep(1)
decrement_two.click()
time.sleep(1)


delete_two = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[2]/div[3]/i[1]")))

delete_two.click()
time.sleep(1)



delete_one = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/i[1]")))

delete_one.click()
time.sleep(2)

driver.quit()




