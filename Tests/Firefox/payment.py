# Tests adding items to cart and then incrementing/decrementing on cart page and deleting of items
from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.alert import Alert
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.firefox.service import Service

driver = webdriver.Firefox(service=Service(executable_path=GeckoDriverManager().install()))
driver.get("http://localhost:3000/login")
driver.maximize_window()
time.sleep(2)


username_search =  driver.find_element(By.ID, 'email_login')
password_search =  driver.find_element(By.ID, 'password_login')
login_btn     =  driver.find_element(By.ID, 'login_click')

time.sleep(2)


time.sleep(2)
username_search.send_keys("brianbhattihp@gmail.com")
time.sleep(2)
password_search.send_keys("India9157190")
time.sleep(2)
login_btn.click()
time.sleep(2)

driver.get("http://localhost:3000/menu")

# Relative XPATH is used

add_to_cart_item_one = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[3]/div[2]/button[1]")))

add_to_cart_item_one.click()

time.sleep(1)

add_to_cart_item_two = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[3]/div[2]/button[1]")))

add_to_cart_item_two.click()

time.sleep(1)

add_to_cart_item_three = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[3]/div[2]/button[1]")))

add_to_cart_item_three.click()

time.sleep(1)

add_to_cart_item_four = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[2]/div[6]/div[1]/div[1]/div[3]/div[2]/button[1]")))

time.sleep(1)

cart = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//a[@id='cart']")))

cart.click()

increment = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/i[1]")))



increment_two = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/i[1]")))



decrement_two = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//body/div[@id='root']/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/i[2]")))
time.sleep(2)
decrement_two.click()
time.sleep(2)




pay_btn = WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(),'Pay Now')]")))
pay_btn.click()
time.sleep(2)




driver.switch_to.frame(frame_reference=driver.find_element(By.CSS_SELECTOR, 'body:nth-child(2) > iframe.stripe_checkout_app:nth-child(4)'))



driver.find_element(By.ID, 'email').send_keys('brianbhattihp@gmail.com')
time.sleep(2)
driver.find_element(By.ID, 'shipping-name').send_keys('Brian Bhatti')
time.sleep(2)
driver.find_element(By.ID, 'shipping-street').send_keys('42 Wallaby Way')
time.sleep(2)
driver.find_element(By.ID, 'shipping-zip').send_keys('12345')
time.sleep(2)
#driver.find_element(By.ID, 'shipping-city').send_keys('Sydney')
#time.sleep(2)
driver.find_element(By.ID, 'submitButton').click()
time.sleep(2)







driver.find_element(By.ID, 'card_number').send_keys('4242')
driver.find_element(By.ID, 'card_number').send_keys('4242')
driver.find_element(By.ID, 'card_number').send_keys('4242')
driver.find_element(By.ID, 'card_number').send_keys('4242')
time.sleep(2)
driver.find_element(By.ID, 'cc-exp').send_keys('12')
driver.find_element(By.ID, 'cc-exp').send_keys('23')
time.sleep(2)
driver.find_element(By.ID, 'cc-csc').send_keys('123')
time.sleep(2)
driver.find_element(By.ID, 'submitButton').click()
time.sleep(10)


driver.quit()
