export const addToCart = (menu, quantity) => (dispatch, getState)=>{

    const  totalPrice = menu.price * quantity
    var cartItem = {
        name : menu.name,
        _id : menu._id,
        image : menu.image,
        quantity : Number(quantity),
        // prices : menu.prices,
        // price : menu.prices * quantity

        price : menu.price,

        totalPrice : totalPrice


    }

    if(cartItem.quantity>10)
    {
        alert("You cannot add more than 10 quantity to cart")
    }
    else
    {
        if(cartItem.quantity<1)
        {
            dispatch({type: 'DELETE_FROM_CART',payload: menu})
        }
        else{
            dispatch({type: 'ADD_TO_CART', payload: cartItem})
        }
        

    }
    const cartItems = getState().cartReducer.cartItems
    localStorage.setItem('cartItems', JSON.stringify(cartItems))


}

export const deleteFromCart=(menu)=>(dispatch, getState)=>{


    dispatch({type: 'DELETE_FROM_CART',payload: menu})
    const cartItems = getState().cartReducer.cartItems
    localStorage.setItem('cartItems', JSON.stringify(cartItems))


}